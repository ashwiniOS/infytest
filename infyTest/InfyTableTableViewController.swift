//
//  InfyTableTableViewController.swift
//  infyTest
//
//  Created by Ashwin Nooli on 30/7/17.
//  Copyright © 2017 AshwinNooli. All rights reserved.
//

import UIKit

class InfyTableTableViewController: UITableViewController {

    var factViewModel = FactViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

//Register the Custom DataCell
       

// Added refresh control to pull and refresh data
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(InfyTableTableViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        self.fetchFacts()
        
        self.tableView.rowHeight = 100.0
        
// Added Navigation Bar to display titile
        
        

    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return factViewModel.numberOfItemsInSection(section: section)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let factsCell = FactTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "FactId")
//        let factsCell = tableView.dequeueReusableCell(withIdentifier: "factID", for: indexPath as IndexPath)   as! FactTableViewCell
//        
        self.factViewModel.configureCell(cell: factsCell, forRowAtIndexPath: indexPath)
        
        return factsCell;
    }

    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
//
//        // Configure the cell...
//
//        return cell
//    }
//    


     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
     func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }




    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: - View and View Model Methods
    
    func fetchFacts()
    {
        
        factViewModel.facts = []
        self.tableView.reloadData()
        factViewModel.fetchFacts(completion: {
            DispatchQueue.main.async(execute: {
                // self.refreshControl.endRefreshing()
                self.tableView.reloadData()
                self.navigationItem.title = self.factViewModel.factTitle
                print(self.factViewModel.factTitle ?? "No Value Found")
            })
            
        })

        
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        // Simply adding an object to the data source for this example
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
        self.fetchFacts()
    }
    

}
