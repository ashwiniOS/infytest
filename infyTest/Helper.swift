//
//  Helper.swift
//  infyTest
//
//  Created by Ashwin Nooli on 30/7/17.
//  Copyright © 2017 AshwinNooli. All rights reserved.
//

import UIKit

struct Constants {
    
    struct ApiConstants {
        
        
        static let baseURLStr = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
        
        
        static let tableLabelColor = UIColor.blue
        static let tableDetailLabelColor = UIColor.black
        static let navigationTintColor = UIColor.blue
        static let backgroundColor = UIColor.white
        
        
    }


    
}


/// Below is to populate imageview with image asyncronously

extension UIImageView {
    public func imageFromServerURL(urlString: URL, defaultImage : String?) {
        if let di = defaultImage {
            self.image = UIImage(named: di)
        }
        
        URLSession.shared.dataTask(with: urlString, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }
}

