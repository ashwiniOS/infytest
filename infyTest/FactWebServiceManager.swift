//
//  FactWebServiceManager.swift
//  infyTest
//
//  Created by Ashwin Nooli on 30/7/17.
//  Copyright © 2017 AshwinNooli. All rights reserved.
//

import Foundation
import SwiftyJSON

/*! @brief factManager is a network manager client to invoke webservice calls to fetch songs from the web api. This gets called by th view model */
class FactsManager: NSObject {

    /**
     This function is used to request data from server.
     - Parameter searchItem: The search item is provided by the view model with the keyword for iTunes search.
     - Returns: either success or failer completion block.
     */
    
    func fetchFactsFromAPI(completion:@escaping ([FactModel]?,String) -> ()) {
        
        
        do {
            let jsonData = try Data.init(contentsOf: URL.init(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")!)
            
            print(jsonData)
            
            if let jsonString = String.init(data: jsonData, encoding: String.Encoding.ascii) {
                print(jsonString)
                
                if let modifiedData : Data = jsonString.data(using: String.Encoding.utf8) {
                    do {
              
                        let json =  JSON(data: modifiedData)
                        
                        var tempList:[FactModel] = []
                        let navTitle = json["title"].string
                        
                        for (key,subJson):(String, JSON) in json["rows"] {
                            //Do something you want
                            print("The value for \(key) is \(subJson)")
                            
                            let title = subJson["title"].string
                            let desc = subJson["description"].string
                            let img = subJson["imageHref"].string
                            if (title != nil){
                           
                                
                                let myFactObj = FactModel(title:title!, description:desc, imageURL:img)
                            
                            tempList.append(myFactObj)
                            }
                            
                        }
                        
                       // print(json) //this part works fine
                         completion(tempList as [FactModel], navTitle! )
                    }
                }
            }
        } catch {
            print("Error")
        }

       
     
     
 
        
        //end of func
    }

    
 
}
