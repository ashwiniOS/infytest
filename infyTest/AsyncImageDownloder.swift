//
//  AsyncImageDownloder.swift
//  b2CloudRss
//
//  Created by Ashwin Nooli on 25/5/17.
//  Copyright © 2017 AshwinNooli. All rights reserved.
//

import UIKit

protocol AsycImageDownloadDelegate {
    func didDownloadImage(forRowAtIndexPath indexPath: IndexPath, image: UIImage)
}

class AsyncImageDownloder: NSObject {
    var delegate : AsycImageDownloadDelegate?
    
    func asyncImageDownload(indexPath: IndexPath, urlString: String)
    {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "error")
                let image = UIImage.init(named: "placeholder")
                self.delegate?.didDownloadImage(forRowAtIndexPath: indexPath, image: image!)
            } else {
                let image = UIImage(data: data!)
                self.delegate?.didDownloadImage(forRowAtIndexPath: indexPath, image: image!)
            }
        }).resume()
    }
}
