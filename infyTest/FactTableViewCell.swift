//
//  FactTableViewCell.swift
//  infyTest
//
//  Created by Ashwin Nooli on 30/7/17.
//  Copyright © 2017 AshwinNooli. All rights reserved.
//

import UIKit
import SnapKit

class FactTableViewCell: UITableViewCell {
    var factImageView:UIImageView!
    var factCellContainerView:UIView!
    var titleLabel:UILabel!
    var detailLabel:UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    private func setup() {
        factImageView = UIImageView()
        
        factCellContainerView = UIView()
        
        titleLabel = UILabel()
        titleLabel.textColor = Constants.ApiConstants.tableLabelColor
        titleLabel.adjustsFontSizeToFitWidth = true
        
        detailLabel = UILabel()
        detailLabel.textColor = Constants.ApiConstants.tableDetailLabelColor
        detailLabel.sizeToFit()
        factCellContainerView.addSubview(titleLabel)
        factCellContainerView.addSubview(detailLabel)
        
        factCellContainerView.addSubview(factImageView)
        contentView.addSubview(factCellContainerView)
        
        
        ///Add constraints using SnapKit
        
        factImageView.snp.makeConstraints({
            (make) -> Void in
            make.top.equalTo(contentView).offset(10)
            make.left.equalTo(contentView).offset(10)
            make.bottom.equalTo(contentView).offset(-10)
            make.width.equalTo(factImageView.snp.height)
        })
        
        factCellContainerView.snp.makeConstraints({
            (make) -> Void in
            make.top.equalTo(contentView).offset(10)
            make.left.equalTo(factImageView.snp.right).offset(10)
            make.bottom.equalTo(contentView).offset(-10)
            make.right.equalTo(contentView).offset(-10)
        })
        
        titleLabel.snp.makeConstraints({
            (make) -> Void in
            make.top.equalTo(factCellContainerView)
            make.left.equalTo(factCellContainerView)
            make.right.equalTo(factCellContainerView)
            make.bottom.equalTo(detailLabel.snp.top).offset(-10)
        })
        
        detailLabel.snp.makeConstraints({
            (make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.left.equalTo(factCellContainerView)
            make.right.equalTo(factCellContainerView)
            make.bottom.equalTo(factCellContainerView)
        })

    }
    

}
