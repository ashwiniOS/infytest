//
//  FactViewModel.swift
//  infyTest
//
//  Created by Ashwin Nooli on 30/7/17.
//  Copyright © 2017 AshwinNooli. All rights reserved.
//

import UIKit


class FactViewModel:NSObject
{

    var factsManager = FactsManager()
    
    /// An array to store the Fact models.
    var facts: [FactModel]?
    var tableView: UITableView?
    var factTitle:String?
    
    override init() {
        /// An intial value for facts array
        self.facts = []
        
    }
    
    
    // MARK: - Server Requests
    
    /**
     This method enables server request to fetch facts from the API
     */
    
    func fetchFacts(completion: @escaping () -> ())
    {
        
        factsManager.fetchFactsFromAPI(completion: {
            
            (facts,title) in
            
            self.facts = facts
            self.factTitle = title
            completion()
        })
        
    }
    
    
    // MARK: - Table View
    
    
    /**
     Below methods provides datasource for the table in the Facts Table View Controller .
     
     */
    func numberOfItemsInSection(section :Int) -> Int {
        
        return facts?.count ?? 0
    }
    
    /**
     Configures cell with fact info.
     
     */
    func configureCell(cell: FactTableViewCell, forRowAtIndexPath indexPath: IndexPath){
        
        
        guard let currentFact = self.facts?[indexPath.row] else {
            //do nothing
            return
        }
        
        print(currentFact.description ?? "")
        cell.titleLabel.text = currentFact.title
        cell.detailLabel.text = currentFact.description ?? ""

        if let imageString = currentFact.imageURL{
        
            cell.factImageView.imageFromServerURL(urlString: URL(string:imageString)!, defaultImage: "")
        }
        
       
    }
    

}
