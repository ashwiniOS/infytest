//
//  factModel.swift
//  infyTest
//
//  Created by Ashwin Nooli on 30/7/17.
//  Copyright © 2017 AshwinNooli. All rights reserved.
//

import Foundation

class FactModel{

    var title:String
    var description:String?
    var imageURL:String?

    
    
    /**
     Initializes a new fact with facts info.
     - Parameters:
     - title: Sets title for each fact.
     - description: sets description for each fact.
     - imageURL: sets url to display image for each fact.
     
     - Returns: A new fact is returned.
     */

    
    init(title:String = "", description:String? = nil, imageURL:String? = nil) {
        self.title = title
        self.description = description
        self.imageURL = imageURL
    }
    
    
}
