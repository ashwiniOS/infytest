//
//  factModelTests.swift
//  infyTest
//
//  Created by Ashwin Nooli on 30/7/17.
//  Copyright © 2017 AshwinNooli. All rights reserved.
//

import XCTest

@testable import infyTest

class factModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func test_init_factTitleShouldSetTitle(){
        let title = "Hello"
        let factObj = FactModel()
        factObj.title = title
        
        XCTAssertEqual(factObj.title, "Hello", "The title should be set")
        
    }
    func test_Init_TakesTitleAndSetsTitile(){
        
        let factObj = FactModel(title:"Hello")
        
        XCTAssertEqual(factObj.title, "Hello", "The title should be set")
    }
    func test_init_factDescriptionShouldSetDescription(){
      
        
        let factObj = FactModel(title:"" ,description:"My Des")
        
        
        XCTAssertEqual(factObj.description, "My Des", "The description should be set")
        
    }
    
    func test_init_factImageShouldSetImageURL(){
        
        
        let factObj = FactModel(title:"" ,imageURL:"some url")
        
        
        XCTAssertEqual(factObj.imageURL, "some url", "The image URL should be set")
        
    }
    
}
